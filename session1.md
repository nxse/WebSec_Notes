# Session 1


## HTML Basics

- `<br />`
- `<img src="" width="100px" height="100px" />`
- Table
    - `<table border="1">`
    - `<tr>`
    - `<td rowspan="" colspan="">`
- `<a href="" />`

## HTML Link Hierarchy

- Same as UNIX Hierarchy
- **Parent folder:** `../`  
  Assume we're in `https://nyan.cat/cats/yellow_cat/`  
  To link to `https://nyan.cat/cats/red_cat/`  
  The href needs to be `../red_cat/`

- **Same folder  :** `./`  
  Assume we're in `https://nyan.cat/cats/`  
  To link to `https://nyan.cat/cats/red_cat/`  
  The href needs to be `./red_cat/`  
  Note that this is completely useless.  
  Because linking to `./red_cat/ is completely the same as linking to red_cat/`

- **Subfolder    :** `folder_name/`  
  Assume we're in `https://nyan.cat/cats/`  
  To link to `https://nyan.cat/cats/red_cat/cute_cat.png`  
  The href needs to be `red_cat/cute_cat.png`


- **Website root :** `/`  
  Assume we're in `https://nyan.cat/cats/yellow_cat/`  
  To link to `https://nyan.cat/dogs/no_dogs_allowed.png`  
  The href needs to be `/dogs/no_dogs_allowed.png`